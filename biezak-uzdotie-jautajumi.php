<?php session_start(); ?>
<!DOCTYPE html lang="">
<html lang="lv">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>IT Risinājumu Jautājumi | web.inovacijuparks.lv</title>
        <meta name="description" content="Mājas lapu un IT risinājumu biežāk uzdotie jautājumi, cenas un izstrāde, SEO un interneta mārketings, dizains un saziņa ar klientu.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,700,900|PT+Sans+Narrow:400,700" rel="stylesheet">
        <link rel="apple-touch-icon" href="apple-touch-iphone.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-ipad.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-iphone4.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-ipad-retina.png" />


   <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133169963-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-133169963-2');
</script>

   
<link rel="icon" type="image/png" href="images/favicon_images/favicon-32x32.png"" sizes="32x32" />
<link rel="icon" type="image/png" href="images/favicon_images/favicon-16x16.png"" sizes="16x16" />
   </head>


    <header class="site-header" role="banner"> 
        <nav id="navbar" class="navbar navbar-expand-sm navbar-light bg-light fixed-top">
                    <a class="navbar-brand mr-auto" href="/">
                        <img src="images/ziplogo fixed.png" id="ZIPlogo" alt="logo" >
                    </a>
                    <button class="navbar-toggler navbar-toggler-right ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                  
                    <div class="collapse navbar-collapse" id="navbarToggler">
                      <div class="navbar-nav ml-auto mt-2">  <!--- ml-auto mt-5"--->
                          <a class="nav-item nav-link navbar-text ml-auto mr-auto" data-scroll href="/#parmums">Par Mums<span class="sr-only">(current)</span></a>
                          <a class="nav-item nav-link navbar-text ml-auto mr-auto" data-scroll href="/#pakalpojumi2">Pakalpojumi</a>
                          <a class="nav-item nav-link navbar-text ml-auto mr-auto" data-scroll href="/#principi2">Darbības Principi</a>
                          <a class="nav-item nav-link navbar-text ml-auto mr-auto" data-scroll href="/#darbi2">Mūsu Darbi</a>
                          <a class="nav-item nav-link navbar-text ml-auto mr-auto" data-scroll href="/#kontakti2">Kontakti</a> 
                    </div>
                </div>
            </nav>
        </header> 
        
        
       
        <div class="content">
            
<!------------------------------------------------------------------------------------------------------------------------->

<!-----------------------------------------------------IMAGE CONTAINER------------------------------------------->


<div class="container-fluid px-0">
  <div class="row mx-0">
    <div class="col-12 px-0">
      <img class="img-fluid w-100" src="images/biezak-uzdotie-jautajumi.png" alt="Biežāk Uzdotie Jautājumi">
    </div>
  </div>  
</div>



<!------------------------------------------------------------------------------------------------------------------->



<!-----------------------------------------------------FAQ CONTAINER------------------------------------------------>

<div class="container-fluid mx-0 py-4 info-text-container">
    <div class="row mx-0 px-0">
     <div class="col-12 mx-auto">
                <h1 class="mx-auto info-header py-4">Biežāk uzdotie jautājumi</h1>
        </div>
    </div>
</div>


<div class="container-fluid py-5 pl-3 pr-5 feature-header-container">
    <div class="row mx-0 px-0">
     <div class="col-lg-6 col-sm-10 col-xs-12 mx-auto">
                <ul class="faq-list-content text-justify fa-ul ">
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Cik ilgi tiek veikta mājas lapas un citu projektu izstrāde?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>Dažādu IT projektu izstrāde ir ļoti individuāls process un ir atkarīgs no tā cik ļoti tas ir sarežģīts, lai Jūsu tas tiktu izveidots. Mēs piedāvājam bezmaksas konsultāciju, lai nonāktu pie galējā laika termiņa un izstrādes ilgumu, kuru cenšamies īstenot pēc iespējas īsākā laikā.</li>
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Kas ir landing page, jeb nolaišanās lapa?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>Landing page ir jebkura web lapa, kurā apmeklētājs ienāk pašā sākumā. Tā ir radīta ar mērķi tās apmeklētāju veicināt uz konkrētu darbību veikšanu.</li>
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Kas ir SEO?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>SEO jeb Search Engine Optimization (latviski Meklētājprogrammas optimizācija) ir ļoti plašs jēdziens un darbību klāsts ar kuru palīdzību nepieciešamais produkts, mājas lapa, web aplikācija u.t.t. tiek padarīta atrodama un atpazīstama meklēšanas rīkos.</li>
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Kas ir Web Aplikācija un kā tā atšķiras no parastas mājas lapas?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>Web aplikāciju var nosaukt par uzlabotu mājas lapu. Tā sevi nodrošina dažādas sarežģītības pakāpes funkcijas, un ir paredzēta lai veiktu dažādus uzdevumus un ne tikai sniegt informāciju vietnes lietotājam.</li>
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Kādas ir Jūsu pakalpojumu izmaksas?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>Sakarā ar to ka katrs projekts ir unikāls, mēs sastādām atsevišķu tāmi katram pasūtījumam, lai tas būtu pēc iespējas izdevīgāks katram mūsu klientam. Vairāk informācijas par katra pakalpojuma izmaksām, mēs sniedzam mūsu bezmaksas konsultācijā.</li>
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Kā apmaksāt?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>Mēs savus darbus veicam ar līguma sadarbību, un apmaksa tiek veikta ar pārskaitījuma palīdzību.</li>
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Vai Jūs nodrošināt garantiju?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>Jā, mēs garantējam jebkādu problēmu novēršanu 1 gada laikā, lai arī Jūs pie mums Jūsu produktu neuzturētu.</li>
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Kas veido jaunos dizainus?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>Mūsu komandā ir vairāki pieredzējuši un profesionāli dizaineri, kas Jums spēs piedāvāt pašus modernākos risinājumus no dizaina pasaules.</li>
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Kā norit izstrādes process?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>Mēs nodrošinām profesionālu IT projektu izstrādes procesu, lai sasniegtu pašus labākos rezultātus. Procesa sākumā notiek saruna ar klientu un prasību izvirzīšana. Pēc tam veicam pilnu analīzi un stratēģijas izvirzīšanu projekta vajadzībām, kas sevī ietver mērķu nospraušanu, tirgus izpēti, konkurences analīzi un citas lietas. Turpinām ar programmēšanas darbiem un satura veidošanu. Atkarībā no projekta vajadzībām izmantojam specializētas tehnikas, kas ir nepieciešams. Process turpinās ar projekta testēšanu un visbeidzot tā palaišanu galējā versijā.</li>
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Kas ir Agile projektu vadība un kāpēc mēs to izmantojam?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>No ierastās projektu vadības “Agile” atšķiras ar to, ka projekts netiek veikts viss uzreiz. Tiek izstrādāts rīcības plāns, kurā tiek noteikti mērķi un prioritātes, un procesu sadala mazos gabalos. Projekta izstrādātāji veidos projekta attīstības plānu pa posmiem. Notiek nepārtraukta komunikācija. Tādā veidā trūkumi, budžets un projekta ilguma svārstības ir samazinātas līdz minimumam. “Agile” palīdz strādāt ātrāk un vieglāk– izvairoties no grūti labojamām kļūdām un retas komunikācijas ar klientu līdz ar ko arī samazinās projekta izmaksas.</li>
                    <li class="mt-4 mb-2 faq-question"><span class="fa-li"><i class="fas fa-question-circle"></i></span>Saziņa ar mums izstrādes procesa laikā?</li>
                    <li class="mt-2 mb-4 faq-answer text-justify"><span class="fa-li"><i class="fas fa-comment"></i></span>Izstrādes procesa laikā nodrošinām bez pārtraukuma saziņu ar klientu, lai visa procesa laikā būtu komunikācija no abām pusēm. Ar Jums kontaktā būs konkrētā projekta vadītājs.</li>

                </ul>   
        </div>
    </div>
    <div id="privatuma_politika2">
</div>
</div>

<!------------------------------------------------------------------------------------------------------->

<!-----------------------------------------------------TOS CONTAINER------------------------------------------->


<div id="privatuma_politika" class="container-fluid mx-0 py-4 principles-header-container">
    <div class="row mx-0 px-0">
     <div  class="col-12 mx-auto">
                <h2 i class="mx-auto info-header py-4">Privātuma Politika</h2>
        </div>
    </div>
</div>

<div class="container-fluid mx-auto px-4 py-3 feature-header-container">
    <div class="row mx-auto px-0">
     <div class="col-lg-6 col-sm-10 col-xs-12 mx-auto motto-text-container tos-text-container">
                <p class="mt-5 mb-4 text-justify">Šī tīmekļa vietne pieder un to uztur Nodibinājums “Zinātnes un Inovāciju Parks”, reģistrācijas numurs: 40008191808, juridiskā adrese Peldu iela 32/34, Liepāja, LV 3401, Latvija, turpmāk ZIP. Esam apņēmušies aizsargāt ZIP rīcībā esošo fizisko personu datu privātumu un apstrādāt, uzkrāt un saglabāt tikai tādus jūsu kā fiziskās personas datus, kas ir tiesiski iegūti un ir nepieciešami mūsu komercdarbības veikšanai, normatīvo aktu prasību un līgumsaistību izpildei. Apstrādājot jūsu personas datus, ZIP veltīs pienācīgu rūpību, lai nodrošinātu to konfidencialitāti, integritāti un drošu uzglabāšanu. ZIP veiks jūsu personas datu apstrādi, piemērojot iekšējās procedūras un mehānismus, lai novērstu neautorizētu personas datu lietošanu, piekļuvi, izpaušanu, kopēšanu, grozīšanu u.t.t..</p>
                <h2 class="mt-5 mb-3">Sīkdatnes</h2>
                <p class="mt-2 mb-4 text-justify">ZIP ir tiesīgs vākt datus par tīmekļa vietnes lietotājiem (tai skatā Jums), izmantojot sīkdatnes (angliski Cookies), lai uzlabotu lietotājiem pieejamos pakalpojumus. Sīkdatnes ir datnes, ko tīmekļa vietnes izvieto lietotāju datoros, lai atpazītu lietotāju un atvieglotu tam vietnes izmantošanu. Interneta pārlūkprogrammas var konfigurēt tā, lai tās brīdina lietotāju par sīkdatņu izmantošanu un ļauj izvēlēties, vai lietotājs piekrīt tās pieņemt. Papildus tam serveri reģistrēs jūsu IP adresi, pārlūkprogrammas veidu, valodu iestatījumu, operētājsistēmu, interneta pakalpojumu sniedzēju un datuma/ laika zīmogu un laiku, kas pavadīts ZIP vietnes sadaļās. Šo informāciju ZIP vāc un izmanto tikai, lai nodrošinātu ērtu un drošu tīmekļa vietnes darbību, kā arī statistikas veidošanas vajadzībām, proti, lai uzzinātu vairāk par ZIP vietnes apmeklētāju darbībām vietnē un analizētu apmeklētāju lietošanas pieredzi.</p>
                <h2 class="mt-5 mb-3">Trešās puses </h2>
                <p class="mt-2 mb-4 text-justify">ZIP nenodos Personas datus trešajām personām, izņemot, ciktāl tas nepieciešams saprātīgai komercdarbības īstenošanai. Taču šādā gadījumā ZIP nodrošinās, ka šīs trešās personas saglabā jūsu Personas datu konfidencialitāti un nodrošina piemērotu aizsardzību.</p>
                <h2 class="mt-5 mb-3">Autortiesības</h2>
                <p class="mt-2 mb-4 text-justify">Vietnes saturs tiek aizsargāts Copyright © web.inovacijuparks.lv. Jebkuras tiesības, kas šeit nav īpaši atrunātas, tiek rezervētas. ZIP ievēro un respektē autoru tiesības un mēs esam atbildīgi par tāda satura dzēšanu, kas aizskar vai pārkāpj vai var aizskart un pārkāpt autoru tiesības.</p>
                <h2 class="mt-5 mb-3">Atruna</h2>
                <p class="mt-2 mb-4 text-justify">Par jebkuras Vietnes informācijas, rīku un pakalpojumu izmantošanu pilnībā ir atbildīgs lietotājs. ZIP neuzņemas nekāda veida atbildību par izmantotās informācijas, rīku un pakalpojumu precizitāti vai funkcionalitāti. Vietnē izvietotā informācija ir paredzēta vienīgi informatīviem nolūkiem. Šī informācija nevar tikt uztverta kā jebkāda veida padoms un tā nevar tikt izmantota par pamatu nekādas darbības uzsākšanai vai lēmumu pieņemšanai.</p>
                <h2 class="mt-5 mb-3">Pielāgojumi un izmaiņas privātuma politikā</h2>
                <p class="mt-2 mb-5 text-justify">ZIP pieņem, ka pirms tīmekļa vietnes izmantošanas, jūs esat izlasījuši Biežāk uzdoto jautājumu un Privātuma politikas sadaļas, kā arī esat akceptējuši tās. ZIP patur tiesības veikt grozījumus šo politiku nosacījumos.</p>
        </div>
    </div>
</div>



<!------------------------------------------------------->


<!-----------------------------------------------------TOS CONTAINER------------------------------------------->


<div class="container-fluid mx-0 py-3 principles-header-container">
    <div class="row mx-0 px-0">
     <div class="col-xl-12 col-lg-12  col-md-12 col-sm-12 col-xs-12 mx-auto">
                <h2 class="mx-auto info-header py-4">Rekvizīti</h2>
        </div>
    </div>
</div>


<div class="container-fluid px-2 mx-auto py-5 feature-header-container">
    <div class="row mx-auto px-0">
     <div class="col-xl-4 col-lg-4  col-md-4 col-sm-12 col-xs-12 mx-auto motto-text-container business-info-text-container text-center">
    <p><b>Nodibinājums:</b> Zinātnes un inovāciju parks</p>
    <p><b>Reģistrācijas numurs:</b> 40008191808</p>
    <p><b>Banka:</b> A/S Swedbank</p> 
    <p><b>Kods:</b> HABALV 22</p>
    <p><b>Konts:</b> LV19HABA0551032996262</p>
</div>
        </div>


  <div class="col-md-2 col-sm-3 col-xs-3 mx-auto mt-5">
      <button id="homeButton" class="btn btn-block btn-primary btn-contact btn-return">Atpakaļ</button>
    </div>
</div>






<!-------------------------------------------------------

   <!------------------------------------------ReCaptcha--------------------------------------------------->
  <script>
   function onloadCallback() {
  grecaptcha.ready(function() {
      grecaptcha.execute('6LeDAY8UAAAAAOYIJoeOpvFOnFhzfmONGBb7D1K0', {action: '/'}).then(function(token) {
      });
  });
   }
  </script>
<!-------------------------------------------------------------------------------------------------->



        
<!------------------------------------------Scroll to top button---------------------------------------------------------->

                <i onclick="scrollToTop()" id="scrollToTopButton" title="Scroll to top" class="fas fa-angle-up"></i>

<!------------------------------------------------------------------------------------------------------------------------>
    



<!-------------------------SCRIPT SOURCES-------------------------------------------->

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="js/js.cookie.js"></script>
<script src="js/script.js"></script>
<script src="js/smooth-scroll.polyfills.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js?render=6LeDAY8UAAAAAOYIJoeOpvFOnFhzfmONGBb7D1K0' crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>



<script>
hideRecaptcha()
</script>



<!--------------------------------------------------------------------------------------------->


<!------------------------------------------LightBox------------------------------------------>

<script>
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
</script>

<!--------------------------------------------------------------------------------------------->

<!-----------------------------------------Cookie Consent Script----------------------------------->
<?php
if (empty($_COOKIE['cookie_accept'])) {
echo "<script>
showAndHideCookieConsent();
</script>";
}
?>
<!--------------------------------------------------------------------------------------------------->

<!--------------------------------------Acceptance Cookie Check-------------------------------------->

<div class="py-4" id="cookieConsent">
   <p class="my-auto">Šī mājaslapa izmanto sīkdatnes. <a data-scroll href="#privatuma_politika">Vairāk informācijas</a></p>
   <button id="cookieConsentOK" class="">Piekrītu</button>
 </div>

<!--------------------------------------------------------------------------------------------------->



<script>
var scroll = new SmoothScroll('a[href*="#"]', {
speed: 400
});
</script>


</body>

    <!--------------------------------------------------------------------------------------------->


    <script type="text/javascript">
        document.getElementById("homeButton").onclick = function () {
            location.href = "/";
        };
    </script>


    <!---------------------------------Footer CONTAINER ----------------------------------------->
    <footer>
<div class="container-fluid my-0 px-0 pt-4 pb-0">

    <div class="row mx-0 my-0">
        <div class="col-lg-12">
        <p class="footer">Liepāja © 2012 - <?php echo date("Y"); ?></p>
            <p>Made By: web.inovacijuparks.lv</p>
            <p>Tālrunis: +371 20044576 </p>
        </div>
    </div>

  
</div>
</footer>

<!----------------------------------------------------------------------------------------------->


</div>
    


  
</html>