// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 2) {
    document.getElementById("scrollToTopButton").style.display = "block";
  } else {
    document.getElementById("scrollToTopButton").style.display = "none";
  }
}

const btn = document.getElementById('scrollToTopButton');


btn.addEventListener('click', () => window.scrollTo({
  top: 0,
  behavior: 'smooth',
}));


function hideRecaptcha() {
    const recaptcha = $(".grecaptcha-badge");
    if (recaptcha.length) return recaptcha.css({ display: "none" });
    requestAnimationFrame(() => this.hideRecaptcha());
}


function showAndHideCookieConsent() {
$(document).ready(function(){   
  setTimeout(function () {
      $("#cookieConsent").fadeIn(200);
   }, 1000);
  $("#cookieConsentOK").click(function() {
    Cookies.set('cookie_accept', 'set', { expires: 7, path: '' });
    $("#cookieConsent").fadeOut(200);
  }); 
});
}







