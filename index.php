<?php session_start(); ?>
<!DOCTYPE html>
<html lang="lv">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Mājas Lapu Izstrāde un IT Risinājumi | web.inovacijuparks.lv</title>
        <meta name="description" content="Profesionāli IT pakalpojumi jebkurai nozarei. Mūsu pieredzējušie darbinieki palīdzēs Jums nonākt pie pareizā risinājuma tieši Jums - mājas lapa, SEO, grafiskais dizains u.c.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
    

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133169963-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-133169963-2');
</script>
<!--------------------------------------------------------------------------------->


<link rel="icon" type="image/png" href="images/favicon_images/favicon-32x32.png"" sizes="32x32" />
<link rel="icon" type="image/png" href="images/favicon_images/favicon-16x16.png"" sizes="16x16" />

</head>



    <body data-spy="scroll" data-target="#navbar">

    <header class="site-header" role="banner"> 
        <nav id="navbar" class="navbar navbar-expand-sm navbar-light bg-light fixed-top">
                    <a class="navbar-brand mr-auto px-0" href="#">
                        <img src="images/ziplogo fixed.png" alt="logo" id="ZIPlogo" >
                    </a>
                    <button class="navbar-toggler navbar-toggler-right ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                  
                    <div class="collapse navbar-collapse" id="navbarToggler">
                      <div class="navbar-nav ml-auto mt-2">  <!--- ml-auto mt-5"--->
                          <a class="nav-item nav-link active navbar-text ml-auto mr-auto" data-scroll href="#parmums">Par Mums<span class="sr-only">(current)</span></a>
                          <a class="nav-item nav-link navbar-text ml-auto mr-auto" data-scroll href="#pakalpojumi">Pakalpojumi</a>
                          <a class="nav-item nav-link navbar-text ml-auto mr-auto" data-scroll href="#principi">Darbības Principi</a>
                          <a class="nav-item nav-link navbar-text ml-auto mr-auto" data-scroll href="#darbi">Mūsu Darbi</a>
                          <a class="nav-item nav-link navbar-text ml-auto mr-auto" data-scroll href="#kontakti">Kontakti</a> 
                    </div>
                </div>
            </nav>
        </header> 
        
        
        <div class="content">
            
<!------------------------------------------------------------------------------------------------------------------------->

 <!-----------------------------------------------Carousel Container---------------------------------------------------->


            <div class="container-fluid px-0 carousel-container" id="parmums">
           <div class="row mx-0">
               <div class="col-12 px-0">
                <div class="frontpage-carousel h-5">
                        <div id="carouselControls" class="carousel slide" data-ride="carousel" data-interval="5000">
                                <ol class="carousel-indicators">
                                  <li data-target="#carouselControls" data-slide-to="0" class="active"></li>
                                  <li data-target="#carouselControls" data-slide-to="1"></li>
                                  <li data-target="#carouselControls" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner text-center">
                                  <div class="carousel-item active">       
                                   <!-- <img class="d-block w-100" id="carousel-image1" src="images/testimages/pic1.png" alt="First slide"> -->                
                                    <img class="d-block w-100" id="carousel-image1" src="images/carousel_images/majaslapuizstrade.png" alt="Mājas Lapu Izstrāde">
                                    <div class="carousel-caption d-md-block carousel-opacity-container">
                                        <div class="carousel-caption carousel-caption1">
                                            <h3 class="carousel-caption-header">Mājas Lapu Izstrāde</h3>
                                                <p class="carousel-caption-text">Profesionālas mājas lapas, ar unikālu dizainu, lai Jūs atšķirtos no vidējās masas un Jūsu bizness paceltos jaunā līmenī</p>
                                        </div>
                                        </div>
                                  </div>

                                  <div class="carousel-item">
                                    <img class="d-block w-100" id="carousel-image2" src="images/carousel_images/itkvalitate.png" alt="IT Kvalitāte">
                                    <div class="carousel-caption d-md-block carousel-opacity-container">
                                    <div class="carousel-caption d carousel-caption2">
                                            <h3 class="carousel-caption-header">Kvalitāte</h3>
                                                <p class="carousel-caption-text">Mūsu profesionālie darbinieki nodrošina ļoti augstu kvalitāti, strādājot IT un dizaina sfērā ilgāk nekā 7 gadus. Sekojam līdzi visām IT pasaules aktivitātēm</p>
                                            </div>
                                          </div>
                                  </div>
                                  
                                  <div class="carousel-item">
                                       <img class="d-block w-100" id="carousel-image3" src="images/carousel_images/itindividualapieeja.png" alt="IT Individuāla Pieeja">
                                       <div class="carousel-caption d-md-block carousel-opacity-container">
                                       <div class="carousel-caption carousel-caption3">
                                            <h3 class="carousel-caption-header">Individuāla Pieeja</h3>
                                                <p class="carousel-caption-text">Katram klientam pieejam atsevišķi ar vislielāko rūpību un kopīgi nonākam pie pareizā risinājuma, kas apmierinātu abas puses. Ticam, ka komunikācija ir pati svarīgākā, lai īstenotu jebkuru projektu!</p>
                                             </div>
                                        </div>
                                    </div>
                                </div>

                                <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>
                                </a>
                              </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        
                
                                
                            
                          
    <!----------------------------------------------Info Text Container---------------------------------------------------->
             
<!---------------------------------------------Info Header--------------------------------------------->     

                <div class="container-fluid px-0 py-4 pt-1 info-text-container">
                <div class="row mx-0 my-3 px-0">

                                <h1 class="mx-auto info-header px-4">Par mums un mūsu IT pieredze</h1>   
                            </div>  
                            <div class="row mx-0 py-1">
                                <p class="mx-auto px-5 info-text text-justify" style="text-indent: 2em;">Mēs esam dažādu interneta un grafiskā dizaina resursu izveides komanda, kas jau vairāk nekā 7 gadus aktīvi un radoši strādā, lai klientiem sniegtu vislabāko un kvalitatīvāko servisu, kas apmierinātu pilnīgi visas prasībās un būtu visaugstākajā līmenī. Komanda nemitīgi attīstās apgūstot jaunākās web pasaules tehnoloģijas, lai visi izstrādātie IT projekti būtu moderni un izmantotu jaunākās tendences IT pasaulē. Mūsu profesionālie darbinieki ir spējīgi īstenot jebkuru Jūsu pasūtījumu, sākot no vienkāršas vizītkartes mājaslapas līdz sarežģītai IT sistēmai, esam gatavi katram izaicinājumam. Piedāvājam bezmaksas konsultāciju, lai Jums palīdzētu saprast, kas tieši ir tas ar ko mēs spēsim Jums palīdzēt.</p>               
                             </div>
                             <div id="pakalpojumi2">
</div>
                     </div>


                         
                    
            
<!--------------------------------------------------------------------------------------------------------------------------->


<!---------------------------------------------FEATURE Header--------------------------------------------->     


                <div id="pakalpojumi" class="container-fluid px-0 pt-4 feature-header-container">
                    <div class="row mx-0 py-5">
                        <h2 class="mx-auto info-header">Pakalpojumi</h2>   
                    </div>
                     </div>
            
<!--------------------------------------------------------------------------------------------------------------------------->



                <!-----------------------feature CONTAINER----------------------------------->

                <div class="container-fluid  px-2 py-4 feature-list-container">
                       <div class="row mx-0 px-0">


                        <div class="col-lg-4 col-xs-12 px-3 py-1 mb-3 mx-0 feature-container">
                                <div class="feature-img-container m-auto">
                                       <img src="images/majaslapuizstrade.png" alt="Mājas Lapu Izstrāde" class="m-auto featureimgs pb-3">
                                </div>
                                <h2 class="feature-header">Mājas Lapu Izstrāde</h2>
                                <p class="text-justify feature-text pt-2 px-3">Dažādu mājas lapu izstrāde, sākot no parastām landing lapām, līdz pilnībā administrējamai lapai izmantojot vadības paneli. Mājas lapas izgatavotas pēc modernākajiem standartiem un ir efektīvas Jūsu mērķu sasniegšanai. </p>
                            </div>


                       <div class="col-lg-4 col-xs-12 px-3 py-1 mb-3 mx-0 feature-container">
                                <div class="feature-img-container">
                                        <img src="images/internetaveikalaizstrade.png" alt="Interneta Veikala Izstrāde" class="m-auto featureimgs pb-3">
                                </div>
                                <h2 class="feature-header">Interneta Veikala Izstrāde</h2>
                                <p class="text-justify feature-text pt-2 px-3">Interneta veikala izstrāde visdažādākajām precēm, jebkādam daudzumam un Jūsu mērķiem. Piedāvājam dažādus pasaulē pazīstamus rīkus ar ko izveidot interneta veikalu, vai arī specializēti izgatavotu veikalu tikai Jums.</p>
                            </div>

                        <div class="col-lg-4 col-xs-12 px-3 py-1 mb-3 mx-0 feature-container">
                                <div class="feature-img-container m-auto">
                                    <img src="images/aplikacijuizstrade.png" alt="Aplikāciju Izstrāde" class="m-auto featureimgs pb-3">
                                </div>
                                <h2 class="feature-header">Aplikāciju Izstrāde</h2>
                                <p class="text-justify feature-text pt-2 px-3">Mobilo un planšetdatoru aplikāciju izstrāde, lai Jūsu bizness varētu pāriet nākamā līmenī. Izstrāde gan uz Android, gan iPhone ierīcēm izmantojot jaunākos risinājumus.</p>
                            </div>

                        <div class="col-lg-4 col-xs-12 px-3 py-1 mb-3 mx-0 feature-container">
                            <div class="feature-img-container m-auto">
                                    <img src="images/seooptimizacija.png" alt="SEO Optimizācija" class="m-auto featureimgs pb-3">
                            </div>
                            <h2 class="feature-header">SEO Optimizācija</h2>
                            <p class="text-justify feature-text pt-2 px-3">Iekšējā un ārējā SEO uzlabošana, SEO stratēģijas plānošana un izveide, mērķauditorijas noteikšana, SEO audits un nepieciešamās korekcijas, lai Jūsu interneta produkti parādītos visaugstākajos rezultātos veicot meklēšanas interneta pārlūkos.</p>
                        </div>
                        

                    <div class="col-lg-4 col-xs-12 px-3 py-1 mb-3 mx-0 feature-container">
                        <div class="feature-img-container m-auto">
                                <img src="images/programmaturasizstrade.png" alt="Programmatūras Izstrāde" class="m-auto featureimgs pb-3">
                        </div>
                        <h2 class="feature-header">Programmatūras Izstrāde</h2>
                        <p class="text-justify feature-text pt-2 px-3">Dažādu sarežģītu sistēmu un programmatūras izstrāde speciāli Jūsu vajadzībām, lai nodrošinātu Jūsu darba efektīvu veikšanu un to atvieglotu. Izstrādājam gan vienkāršas programmas, gan sarežģītas, viss atkarīgs no Jūsu prasībām.</p>
                    </div>
                                                                                                                        
                <div class="col-lg-4 col-xs-12 px-3 py-1 mb-3 mx-0 feature-container">
                    <div class="feature-img-container m-auto">
                           <img src="images/webmarketings.png" alt="WEB Mārketings" class="m-auto featureimgs pb-3">
                    </div>
                    <h2 class="feature-header">WEB Mārketings</h2>
                    <p class="text-justify feature-text pt-2 px-3">Sociālo tīklu reklāmu izveide un to uzturēšana, reklāmas tekstu veidošana, WEB pārdošanas kanālu izveide un pilnveidošana, konkurences izpēte un mārketinga stratēģijas izveide.</p>
                </div>

                <div class="col-lg-4 col-xs-12 px-3 py-1  mb-3 mx-0 feature-container">
                    <div class="feature-img-container m-auto">
                           <img src="images/grafiskaisunwebdizains.png" alt="Grafiskais un WEB Dizains" class="m-auto featureimgs pb-3">
                    </div>
                    <h2 class="feature-header">Grafiskais un WEB Dizains</h2>
                    <p class="text-justify feature-text pt-2 px-3">Grafiskie pakalpojumi no mūsu dizaineru puses - dažādu maketu veidošana, baneru izveide, animācijas, web dizains, aplikāciju dizains u.t.t.</p>
                </div>

                <div class="col-lg-4 col-xs-12 px-3 py-1 mb-3 mx-0 feature-container">
                    <div class="feature-img-container m-auto">
                           <img src="images/majaslapuuzturesana.png" alt="Mājas Lapu Uzturēšana" class="m-auto featureimgs pb-3">
                    </div>
                    <h2 class="feature-header">Mājas lapu Uzturēšana</h2>
                    <p class="text-justify feature-text pt-2 px-3">Mājas lapu uzturēšana pēc tās izveides pie mums, vai arī tās migrēšana uz mūsu servisu, lai mēs par to parūpētos. Domēna reģistrācija, personalizēta e-pasta izveide un citi pakalpojumi saistībā ar hostēšanu.</p>
                </div>

                <div class="col-lg-4 col-xs-12 px-3 py-1 mb-3 mx-0 feature-container">
                    <div class="feature-img-container m-auto">
                           <img src="images/uznemumakorporativaidentitate.png" alt="feature9" class="m-auto featureimgs pb-3">
                    </div>
                    <h2 class="feature-header">Uzņēmuma Korporatīvā Identitāte</h2>
                    <p class="text-justify feature-text pt-2 px-3">Pilna spektra uzņēmuma identitātes izstrāde - krāsu saskaņošana, fonta izvēle, logo izveide, simbolika, tēla veidošana, uzņēmuma identitātes izveide uz dažādiem komunikācijas rīkiem (veidlapas, vizītkartes, atklātnes, līgumi u.c.).</p>
                </div>
            </div>
            <div id="principi2">
</div>
    </div>

<!-------------------------------------------------------------------------------------------------------->


<!-----------------------------------------------------Principles CONTAINER------------------------------------------------>


                     <div id="principi" class="container-fluid px-0 py-4 principles-header-container">
         <div class="row mx-0 px-0">
            <div class="col-12 pt-4 mx-auto planguages-text-container">
                <h2 class="mx-auto info-header py-3">Darbības Principi</h2>
                <p class="mx-auto info-text px-5 my-3">Savas darbības procesā sekojam līdzi visām jaunākajām tehnoloģijām un tās ieviešam savā praksē, lai spētu piedāvāt pēc iespējas profesionālākus un modernākus darbus mūsu klientiem. Mūsu darbinieki ikdienas darbā izmanto šādas tehnoloģijas:</p>
        </div>
    </div>


<div class="container-fluid px-3 my-4 planguages-container">
    <div class="row mx-auto px-0">



        <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/html5_icon.png" alt="HTML5 izstrāde" title="HTML" class="plangicons m-auto">
    </div>
    </div>

   <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/css3_icon.png" alt="CSS izstrāde" title="CSS" class="plangicons m-auto">
        </div>
</div>


    <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/js_icon.png" alt="JS izstrāde" title="JavaScript" class="plangicons m-auto">
        </div>
</div>

        <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/php_icon.png" alt="PHP izstrāde" title="PHP" class="plangicons m-auto pt-3">
        </div>
</div>

    <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/mysql_icon.png" alt="MySQL izstrāde" title="MySQL" class="plangicons m-auto">
        </div>
</div>


    <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/wordpress_icon.png" alt="Wordpress izstrāde" title="WordPress" class="plangicons m-auto">
        </div>
</div>

          <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/materialize_icon.png" alt="Materialize izstrāde" title="Materialize" class="plangicons m-auto">
        </div>
</div>

<div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/mongodb_icon.png" alt="MongoDB izstrāde" title="MongoDB" class="plangicons m-auto">
        </div>
</div>

    <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/bootstrap_icon.png" alt="Bootstrap izstrāde" title="Bootstrap" class="plangicons m-auto pt-1">
    </div>
</div>


    <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/django_icon.png" alt="Django izstrāde" title="Django" class="plangicons m-auto">
        </div>
</div>


    <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/shopify_icon.png" alt="Shopify izstrāde" title="Shopify" class="plangicons m-auto">
        </div>
</div>

          <!--      <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6 mx-0 my-4 plang-icon-container">
                <img src="/images/plang_images/git_icon.png" alt="Git icon" class="plangicons">
        </div>
-->

         <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/react_icon.png" alt="React izstrāde" title="React" class="plangicons m-auto">
        </div>
</div>


    <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/opencart_icon.png" alt="Opencart izstrāde" title="OpenCart" class="plangicons m-auto">
        </div>
</div>

    <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/laravel_icon.png" alt="Laravel izstrāde" title="Laravel" class="plangicons m-auto">
        </div>
</div>


     <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/cplusplus_icon.png" alt="C++ izstrāde" title="C++" class="plangicons m-auto">
        </div>
</div>

    <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/csharp_icon.png" alt="C# izstrāde" title="C#" class="plangicons m-auto">
        </div>
</div>


    <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/java_icon.png" alt="Java izstrāde" title="Java" class="plangicons m-auto">
        </div>
</div>

        <div class="col-md-2 col-4 mx-0 my-4 plang-icon-container">
        <div class="feature-img-container mx-auto">
                <img src="/images/plang_images/vue_js_icon.png" alt="Vue js izstrāde" title="Vue JS" class="plangicons m-auto">
        </div>
</div>


    </div>
</div>


    <div class="row mx-0 my-5 px-0">
     <h2 class="mx-auto why-info-header py-1">Kāpēc ir vajadzīgas IT tehnoloģijas?</h2>   
        </div>
     <div class="row mx-0 py-1">
     <p class="mx-auto px-5 info-text  text-justify" style="text-indent: 2em;">Mērķis, kuru cenšas sasniegt jebkura kompānija, kura nodarbojas ar jebkuru biznesa veidu ir viens – tas ir peļņas gūšana. Lai to sasniegtu, tiek izmantoti dažādi starpposmu līdzekļi, bet konkrētāk – jāpanāk, lai uzņēmums tiek atpazīts, svarīga ir partneru meklēšana, efektīvas struktūras radīšana, u.t.t. Lai sasniegtu šos mērķus, dažādas kompānijas izmanto dažādus biznesa paņēmienus. Varam droši teikt, dažādu IT rīku izmantošana savā biznesā ir ļoti parocīga jebkuram, jo šie pakalpojumi ir pieejami 24/7. Ar IT rīku palīdzību mēs Jums palīdzēsim būt saskarsmē ar Jūsu mērķauditoriju visu laiku. Atvieglo savu dzīvi ar mūsu piedāvātajiem pakalpojumiem.</p>        

     </div>
     <div id="darbi2">
</div>       
                    


</div>


</div>



<!------------------------------------------------------------------------------------------------------------->



<!--------------------------------Past Works Text Container---------------------------------------------------->
             
                    <!--  <div id="worksScroll" class="container-fluid px-0 py-4 pworks-header-container">
                            <div class="row mx-0 px-0">
                                <h2 class="mx-auto info-header py-1">Kāpēc ir vajadzīgas IT tehnoloģijas?</h2>   
                            </div>
                            <div class="row mx-0 py-1">
                                <p class="mx-auto px-5 info-text text-justify">Mērķis, kuru cenšas sasniegt jebkura kompānija, kura nodarbojas ar jebkuru biznesa veidu ir viens – tas ir peļņas gūšana. Lai to sasniegtu, tiek izmantoti dažādi starpposmu līdzekļi, bet konkrētāk – jāpanāk, lai uzņēmums tiek atpazīts, svarīga ir partneru meklēšana, efektīvas struktūras radīšana, u.t.t. Lai sasniegtu šos mērķus, dažādas kompānijas izmanto dažādus biznesa paņēmienus. Varam droši teikt, dažādu IT rīku izmantošana savā biznesā ir ļoti parocīga jebkuram, jo šie pakalpojumi ir pieejami 24/7. Ar IT rīku palīdzību mēs Jums palīdzēsim būt saskarsmē ar Jūsu mērķauditoriju visu laiku. Atvieglo savu dzīvi ar mūsu piedāvātajiem pakalpojumiem.</p>               
                             </div>
                             </div>
                    
                             ->>
<------------------------------------------------------------------------------------------------------------>

                     <div id="darbi" class="container-fluid pt-5 feature-header-container">
                            <div class="row mx-0 px-0 py-4">
                                <h2 class="mx-auto info-header">Mūsu darbi</h2>   
                            </div>
</div>






<!--------------------------------Past Works List Container---------------------------------------------------->

   <div class="container-fluid py-3 pb-4  past-works-list-container">
        <div class="row mx-0 px-0">

            <div class="col-sm-6 col-xs-12 my-4 past-work-container">
                <div class="past-work-img-container mx-auto">
                        <a href="images/pwork_images/grafika.png" alt="Grafika.lv Mājaslapa" data-toggle="lightbox">
                      <img src="images/pwork_images/grafika.png" alt="Grafika.lv Mājaslapa" class="mx-auto pworkimgs">
                            </a>
                </div>
                
                <h2 class="past-work-header mt-4">  <a href="http://grafika.lv" target="_blank">Grafika.lv</a> </h2>
            </div>

            <div class="col-sm-6 col-xs-12 my-4 past-work-container">
                <div class="past-work-img-container mx-auto hoverback">
                        <a href="images/pwork_images/manazemeskaista.png" alt="Manazemeskaista.lv Majaslapa" data-toggle="lightbox">
                        <img src="images/pwork_images/manazemeskaista.png" alt="Manazemeskaista.lv Majaslapa" class="pworkimgs hoverfront">
                            </a>
                </div>
                <h2 class="past-work-header mt-4">  <a href="https://manazemeskaista.lv" target="_blank">LV100</a> </h2>
            </div>

            <div class="col-sm-6 col-xs-12 my-4  past-work-container">
                <div class="past-work-img-container mx-auto">
                        <a href="images/pwork_images/epusdienas.png" alt="Epusdienas.lv Mājaslapa" data-toggle="lightbox">
                    <img src="images/pwork_images/epusdienas.png" alt="Epusdienas.lv Mājaslapa" class="pworkimgs"> 
                            </a>
                </div>
                <h2 class="past-work-header mt-4">  <a href="https://epusdienas.lv" target="_blank">E-Pusdienas</a> </h2>
            </div>

            <div class="col-sm-6 col-xs-12 my-4 past-work-container">
                <div class="past-work-img-container mx-auto">
                        <a href="images/pwork_images/eglieni.png" alt="Eglieni.lv Mājaslapa" data-toggle="lightbox">
                        <img src="images/pwork_images/eglieni.png" alt="Eglieni.lv Mājaslapa" class="pworkimgs"> 
                            </a>
                </div>
                <h2 class="past-work-header mt-4">  <a href="https://eglieni.lv" target="_blank">Eglieni.lv</a> </h2>
            </div>
        </div>

<div id="kontakti2">
</div>

</div>

<!-------------------------------------------------------------------------------------------------------------------------->


<!-------------------------------------------------Contact CONTAINER----------------------------------------------------------->
       

    <div id="kontakti" class="container-fluid px-0 py-5 mx-0  contact-container">
<div class="row mx-0 ">
    <div class="col-12  my-4 mx-0 contact-header-container">
        <h2 class="contact-header-text">Kontakti</h2>
    </div>
</div>

<div class="inputFieldsContainer mx-auto">


    <div class="row mx-0">
    <div class="col-12 mx-auto contact-message-container">
        <p class="info-text message-info-text px-5 mb-4 pb-5">Vēlies bezmaksas konsultāciju, vai ir nepieciešama papildus informācija? Sazinies ar mums web@inovacijuparks.lv vai arī izmantojot formu.</p>
</div>
    </div>

    <form action="contact.php" method="POST">
      
    <?php

    if (isset($_SESSION['mail_status']) && !empty($_SESSION['mail_status'])) {
        echo '
        <div class="row mx-2 px-3">
            <h3 class="mail-response-text text-center mx-auto pb-5">'.$_SESSION['mail_status'].'</h3>
        </div>';

        unset($_SESSION['mail_status']);
    }
    ?>

        
        <div class="row py-3 mx-0">

            <div class="col-sm-4 col-xs-4 mx-auto ">
                       <div class="form-group contactForms">
                        <label class="contact-label-text" for="inputName">Vārds*</label>
                        <input type="text" name="name" class="form-control contactInputText" style="color:#003e6b;" id="inputName" aria-describedby="nameHelp" placeholder="Ievadiet savu vārdu*" required oninvalid="this.setCustomValidity('Lūdzu ievadiet savu vārdu.')" oninput="setCustomValidity('')">
                 </div>
             </div>
          

   
                <div class="col-sm-4 col-xs-4 mx-auto">
                    <div class="form-group contactForms">
                    <label class="contact-label-text" for="inputEmail">E-pasta Adrese*</label>
                    <input type="email" name="email" class="form-control contactInputText" minlenght="8" style="color:#003e6b;" id="inputEmail" aria-describedby="emailHelp" placeholder="Ievadiet savu e-pasta adresi*" required oninvalid="this.setCustomValidity('Lūdzu ievadiet korektu e-pastu')" oninput="setCustomValidity('')"> 
             </div>
             </div>
      

    
                <div class="col-sm-4 col-xs-4 mx-auto">
                    <div class="form-group contactForms">
                    <label class="contact-label-text" for="inputMessage">Telefona Numurs</label>
                    <input type="text" name="phone" class="form-control contactInputText" minlength="8" style="color:#003e6b;" id="inputPhone" aria-describedby="messageHelp" placeholder="Ievadiet savu telefona numuru" oninput="setCustomValidity('')">
             </div>
            </div>


    
            
                        <div class="col-12 mx-auto">
                                <div class="form-group contactForms mx-auto py-3">
                                        <label class="contact-label-text" for="inputMessage">Jūsu Ziņa*</label>
                                        <textarea name="message" cols="40" rows="4" style="color:#003e6b;" class="form-control contactInputText"  id="messageTextArea" aria-describedby="messageHelp" placeholder="Ievadiet savu ziņojumu*" required oninvalid="this.setCustomValidity('Lūdzu ievadiet savu ziņojumu')" oninput="setCustomValidity('')"></textarea>
                                 </div>
                        </div>
            
                        <div class="col-xl-4 col-lg-4  col-md-4 col-sm-4 col-xs-4 mx-auto">
                            <div class="form-group contactButtonDiv px-2 py-2">
                                  <button type="submit" class="btn btn-block btn-primary btn-contact">Nosūtīt</button>
                            </div>
                        </div>
                        </div>
                   
            
                </form>


    <div class="row mx-0 px-0 mx-0">
    <div class="col-12">
              <p class="info-text contact-hint-text">* - Obligāta informācija</p>
              </div>
             </div>

            </div>

            </div>

          
        </div>
    </div>
</div>




<!------------------------------------------------------------------------------------------------------------------------>


<!------------------------------------------Scroll to top button---------------------------------------------------------->

                <i onclick="scrollToTop()" id="scrollToTopButton" title="Scroll to top" class="fas fa-angle-up"></i>

<!------------------------------------------------------------------------------------------------------------------------>
    



<!-------------------------SCRIPT SOURCES-------------------------------------------->

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="js/script.js"></script>
<script src="js/js.cookie.js"></script>
<script src="js/smooth-scroll.polyfills.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src='https://www.google.com/recaptcha/api.js?render=6LeDAY8UAAAAAOYIJoeOpvFOnFhzfmONGBb7D1K0' crossorigin="anonymous"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js'></script>





<script>
var scroll = new SmoothScroll('a[href*="#"]', {
speed: 400
});

hideRecaptcha()
</script>

<script>
$('body').scrollspy({ target: '#navbar', offset: 100 })
</script>

<!--------------------------------------------------------------------------------------------->

<!------------------------------------------LightBox------------------------------------------>

<script>
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
</script>

<!--------------------------------------------------------------------------------------------->


   <!------------------------------------------ReCaptcha--------------------------------------------------->

  <script>
   function onloadCallback() {
  grecaptcha.ready(function() {
      grecaptcha.execute('6LeDAY8UAAAAAOYIJoeOpvFOnFhzfmONGBb7D1K0', {action: '/'}).then(function(token) {
      });
  });
   }
  </script>

<!-------------------------------------------------------------------------------------------------->
<!-----------------------------------------Cookie Consent Script----------------------------------->


<?php
if (empty($_COOKIE['cookie_accept'])) {
echo "<script>
showAndHideCookieConsent();
</script>";
}
?>


<!--------------------------------------------------------------------------------------------------->
        


<!--------------------------------------Acceptance Cookie Check-------------------------------------->



    <div class="py-4" id="cookieConsent">
   <p class="my-auto">Šī mājaslapa izmanto sīkdatnes. <a data-scroll href="biezak-uzdotie-jautajumi#privatuma_politika2">Vairāk informācijas</a></p>
   <button id="cookieConsentOK" class="">Piekrītu</button>
 </div>


<!--------------------------------------------------------------------------------------------->



</body>

<!--------------------------------------------------------------------------------------------->


    <!---------------------------------Footer CONTAINER ----------------------------------------->

<footer>
<div class="container-fluid my-0 px-0 pt-4 pb-0">

    <div class="row mx-0 my-0">



        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mmx-auto">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mx-auto">
           <p style="font-weight: bold; font-size: 1.25em;"> <a class="biezak-uzdotie-jautajumi-link" href="biezak-uzdotie-jautajumi">Biežāk uzdotie jautājumi</a></p> 
        </div>
            <p class="footer">Liepāja © 2012 - <?php echo date("Y"); ?></p>
            <p>Made By: web.inovacijuparks.lv</p>
            <p>Tālrunis: +371 20044576 </p>
        </div>




    </div>

</div>


</footer>

<!----------------------------------------------------------------------------------------------->


</div>
    


  
</html>